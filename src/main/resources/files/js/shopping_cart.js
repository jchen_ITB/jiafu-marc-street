document.getElementById("payform").addEventListener("submit", (e) => {
    e.preventDefault();

    let ok = true;
    let payment_way = document.getElementsByClassName("payment_tab active")[0].getAttributeNode("data-payment-way").value;
    let address_way = document.getElementsByClassName("address_tab active")[0].getAttributeNode("data-address-way").value;
    let saved_address = document.forms.payform.elements.address_saved
    let saved_payment = document.forms.payform.elements.payment_saved

    document.getElementById("payment_way").value = payment_way;
    document.getElementById("address_way").value = address_way;

    if(payment_way === "new"){
        if(!document.getElementById("payment_owner").value.length > 0) ok = false
        if(!document.getElementById("payment_card_num").value.length > 0) ok = false;
        if(!document.getElementById("payment_cvv").value.length > 0) ok = false;
        if(!document.getElementById("payment_exp").value.length > 0) ok = false;

        if(!ok) alert("Rellena el formulario del pago")

    }else if(payment_way === "saved"){

        if(saved_payment.length < 1){
            alert("No tienes ninguna tarjeta guardada")
            ok = false
        }

        if(!Array.from(saved_payment).find(payment => payment.checked)){
            alert("Selecciona una tarjeta guardada")
            ok = false
        }
    }

    if(address_way === "new"){
        if(!document.getElementById("address_street").value.length > 0) ok = false;
        if(!document.getElementById("address_city").value.length > 0) ok = false;
        if(!document.getElementById("address_country").value.length > 0) ok = false;
        if(!document.getElementById("address_zip_code").value.length > 0) ok = false;

        if(!ok) alert("Rellena el formulario de la direccion")

    }else if(address_way === "saved"){

        if(saved_address.length < 1){
            alert("No tienes ninguna direccion guardada")
            ok = false
        }

        if(!Array.from(saved_address).find(address => address.checked)){
            alert("Selecciona una direccion guardada")
            ok = false
        }

    }

    if(ok){
        document.forms.payform.submit();
    }
});