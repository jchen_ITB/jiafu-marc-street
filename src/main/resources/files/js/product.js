function checkQty() {
    let inpObj = document.getElementById("product_qty");

    if (inpObj.value < 1) {
        inpObj.setCustomValidity("La cantidad minima es 1");
    } else {
        inpObj.setCustomValidity("");
    }
}
