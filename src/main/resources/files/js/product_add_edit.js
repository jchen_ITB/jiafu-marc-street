document.getElementById("product_no_image").addEventListener("change", (e) => {
    let fileInput = document.getElementById("product_image");
    if (e.target.checked) {
        fileInput.value = ""
        fileInput.disabled = true;
    } else {
        fileInput.disabled = false;
    }
});
