function validatePassword() {
    let password = document.getElementById("passwd");
    let confirm_password = document.getElementById("confirm_passwd");

    if (password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
        confirm_password.setCustomValidity("");
    }
}
