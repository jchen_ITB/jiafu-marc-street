function validatePassword() {
    let password = document.getElementById("passwd");
    let confirm_password = document.getElementById("confirm_passwd");

    if (password.value != confirm_password.value) {
        confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
        confirm_password.setCustomValidity("");
    }
}

document.getElementById("user_no_avatar")?.addEventListener("change", (e) => {
    let fileInput = document.getElementById("user_avatar");
    if (e.target.checked) {
        fileInput.value = ""
        fileInput.disabled = true;
    } else {
        fileInput.disabled = false;
    }
});

document.getElementById("change_passwd").addEventListener("change", (e) => {
    let password = document.getElementById("passwd");
    let confirm_password = document.getElementById("confirm_passwd");
    if (e.target.checked) {
        password.disabled = false;
        confirm_password.disabled = false;
    } else {
        password.disabled = true;
        confirm_password.disabled = true;
        password.value = "";
        confirm_password.value = "";
    }
});