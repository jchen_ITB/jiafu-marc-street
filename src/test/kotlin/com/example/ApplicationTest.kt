package com.example

import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.http.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.client.statement.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.sessions.*
import kotlin.test.*
import io.ktor.server.testing.*
import itb.jiafumarc.street.dao.DAOUser
import itb.jiafumarc.street.dao.repository.UserRepository
import itb.jiafumarc.street.dao.utils.DatabaseFactory
import itb.jiafumarc.street.models.User
import itb.jiafumarc.street.models.UserSession
import itb.jiafumarc.street.plugins.*
import kotlinx.coroutines.runBlocking
import java.nio.file.Paths
import javax.xml.crypto.Data
import kotlin.time.Duration

class ApplicationTest {
    @Test
    fun testRoot() = testApplication {
        val response = client.get("/")
        assertEquals(HttpStatusCode.OK, response.status)
    }


}